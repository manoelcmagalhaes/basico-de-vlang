fn main(){
	areas := ['suporte', 'academico', 'TI']
	loopa(areas)
	tamanho(areas)
}
//Para arrays a chave vem antes do tipo
fn loopa(x []string){
	println(x[1])
}
//Tamanho do array usa o .len
fn tamanho(x []string){
	println("O tamanho é: ${x.len}")
}