import net.http
import net.html

fn main(){
	response := http.get('https://pt.wikipedia.org/wiki/Wikip%C3%A9dia:P%C3%A1gina_principal') or {panic(err)}
	html_doc := html.parse(response.body)
	all_trs := html_doc.get_tag('tbody')[0].get_tags('tr')
	comp_names := all_trs.map(get_company_name(it))
	println(comp_names)
	println(comp_names.len)
}
fn get_company_name(tr html.Tag) string{
	for uppertag in tr.get_tags('td'){
		for tag in uppertag.get_tags('text'){
			println(tag.content)
		}
	}
	return tr.get_tags('td')[1].get_tags('text')[0].content
}
