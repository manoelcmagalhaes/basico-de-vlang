fn main(){
	var1 := 2
	println("$var1 por extenso é: ${seletor(var1)}")
}
fn seletor(x int) string{
	//Match é o equivalente de switch
	//É necessário ter um else no final
	match x {
		1 {
			return "um"
		}
		2{
			return "dois"
		}
		else {
			return "sei lá"
		}
	}
}