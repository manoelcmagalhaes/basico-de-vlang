import db.sqlite

// sets a custom table name. Default is struct name (case-sensitive)
[table: 'customers']
struct Customer {
    id        int    [primary; sql: serial] // a field named `id` of integer type must be the first field
    name      string
    nr_orders int
    country   string
}

//Criação da tabela com base em uma struct
db := sqlite.connect('dados/customers.db')!

sql db {
    create table Customer
}

// select count(*) from customers
nr_customers := sql db {
    select count from Customer
}
println('number of all customers: $nr_customers')

// V syntax can be used to build queries
uk_customers := sql db {
    select from Customer where country == 'uk' && nr_orders > 0
}
println(uk_customers.len)
for customer in uk_customers {
    println('$customer.id - $customer.name')
}

// by adding `limit 1` we tell V that there will be only one object
customer := sql db {
    select from Customer where id == 1 limit 1
}

println('$customer.id - $customer.name')

// insert a new customer
new_customer := Customer{
    name: 'M'
    nr_orders: 10
	country: 'uk'
}

sql db {
    insert new_customer into Customer
}