fn main(){
	variaveis()
}
fn variaveis(){
	teste := "Variaveis criadas sem mut são imutaveis"
	println("Conteudo da variavel teste: \""+teste+"\"")
	mut segundoteste := "Variaveis criadas com mut podem ter seu conteudo alterado"
	println("Conteudo da variavel segundoteste: \""+segundoteste+"\"")
	segundoteste = "O conteudo foi modificado"
	println("Conteudo da variavel segundoteste: \""+segundoteste+"\"")
}