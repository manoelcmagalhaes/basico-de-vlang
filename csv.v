import os

fn main() {
    filename := 'arrays.v'
    data := os.read_file(filename) or {
        panic('error reading file $filename')
        return
    }
	for linha in data.split('\n'){
		for coluna in linha.split(';'){
			println(coluna)
		}
	}
}
