fn main(){
	//Atentar para o uso dos dois pontos
	saida := parouimpar(11)
	println(saida)
}
fn parouimpar(x int) string{
	if x % 2 == 0 {
		return "$x é par"
	} else {
		return "$x é impar"
	}
}